from django.conf.urls import url, include
from rest_framework import routers
from perris_app import views
from django.contrib import admin
from django.urls import path, include


router = routers.DefaultRouter()
router.register(r'personas', views.PersonaViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [    
    url(r'^', include('perris_app.urls')), 
    url(r'^admin/', admin.site.urls),    
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/', include('social_django.urls', namespace='social')), 
]