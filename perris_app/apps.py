from django.apps import AppConfig


class PerrisAppConfig(AppConfig):
    name = 'perris_app'
