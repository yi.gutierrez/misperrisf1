from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Persona, Mascota

class UserSerializer(serializers.Serializer):
    username =serializers.CharField()
    password =serializers.CharField()
    email =serializers.CharField()
    
    def create(self, validated_data): 
        return User(**validated_data)

class MascotaSerializer(serializers.Serializer):
    nombre = serializers.CharField() 
    raza = serializers.CharField() 
    foto = serializers.ImageField()
    descripcion = serializers.CharField() 
    estado =  serializers.CharField() 

    def create(self, validated_data): 
        return Mascota(**validated_data)
   

class PersonaSerializer(serializers.Serializer):
    
    email = serializers.CharField()    
    contrasenia = serializers.CharField()
    run = serializers.CharField()
    nombre_completo = serializers.CharField()
    fechanac = serializers.DateField()
    telefono = serializers.CharField()
    regiones = serializers.CharField()
    comunas = serializers.CharField()
    tipo_vivienda = serializers.ChoiceField(choices=Persona.CATEGORIES_CHOICES)
   
    class Meta:
        model = Persona
        fields = ('email','run','nombre_completo','fechanac','telefono','regiones','comunas','tipo_vivienda','contrasenia')


    def create(self, validated_data):        
        usuario=validated_data.get('email')
        password=validated_data.get('contrasenia')
        user = User.objects.create_user(usuario, usuario, password)
        usuario_data = User.objects.get(username=usuario)
        persona = Persona.objects.create(usuario=usuario_data, **validated_data)
        return persona


    def update(self, instance, validated_data):
        """
        Update and return an existing `Serie` instance, given the validated data.
        """
        instance.email = validated_data.get('email', instance.email)
        instance.run = validated_data.get('run', instance.run)
        instance.nombre_completo = validated_data.get('nombre_completo', instance.nombre_completo)
        instance.fechanac = validated_data.get('fechanac', instance.fechanac)
        instance.telefono = validated_data.get('telefono', instance.telefono)
        instance.regiones = validated_data.get('regiones', instance.regiones)
        instance.comunas = validated_data.get('comunas', instance.comunas)
        instance.tipo_vivienda = validated_data.get('tipo_vivienda', instance.tipo_vivienda)
        instance.contrasenia = validated_data.get('contrasenia', instance.contrasenia)
        instance.save()
        return instance