from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class Persona(models.Model):

    CASA1 = 'Casa con Patio Grande'
    CASA2 = 'Casa con Patio Pequeño'
    CASA3 = 'Casa sin Patio'
    DEPTO = 'Departamento'

    CATEGORIES_CHOICES = (
        (CASA1, 'Casa con Patio Grande'),
        (CASA2, 'Casa con Patio Pequeño'),
        (CASA3, 'Casa sin Patio'),
        (DEPTO, 'Departamento'),
    )

    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(blank=False, max_length=100, primary_key=True, verbose_name='Correo Electrónico')
    run = models.CharField(max_length=12, verbose_name='Run')
    nombre_completo = models.CharField(max_length=50, verbose_name='Nombre Completo')
    fechanac = models.DateField( verbose_name='Fecha de Nacimiento')
    telefono = models.IntegerField(verbose_name='Teléfono')
    regiones = models.CharField(max_length=40, verbose_name='Región')
    comunas = models.CharField(max_length=40, verbose_name='Comuna')
    tipo_vivienda = models.CharField(max_length=40, choices=CATEGORIES_CHOICES, verbose_name='Tipo de Vivienda')
    contrasenia = models.CharField(blank=False, max_length=20, default=None, verbose_name='Contraseña')
    
    def __str__(self):
        return self.email


class Mascota(models.Model):


    DISP = 'Disponible'
    ADOP = 'Adoptado'
    RESC = 'Rescatado'

    ESTADO_CHOICES = (
        (DISP, 'Disponible'),
        (ADOP, 'Adoptado'),
        (RESC, 'Rescatado'),
    )

    nombre = models.CharField(max_length = 60, verbose_name='Nombre')
    raza = models.CharField(max_length = 40, verbose_name='Raza')
    foto = models.ImageField(upload_to = 'fotos/', verbose_name='Fotografía')
    descripcion = models.CharField(max_length = 200, verbose_name='Descripción')
    estado =  models.CharField(max_length=40, choices=ESTADO_CHOICES, verbose_name='Estado')
   
    def __str__(self):
        return self.nombre
