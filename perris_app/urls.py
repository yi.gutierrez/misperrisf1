from django.conf.urls import url, include
from rest_framework import routers
from . import views
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'index', views.index, name='index'),
    url(r'registro',views.registro, name='registro'), 
    url(r'acceso',views.acceso, name='acceso'), 
    url(r'cerrar',views.cerrar, name='cerrar'),    
    path('rescates/', views.rescates_ver, name="rescates_ver"),    
    path('rescates/new/', views.rescate_nuevo, name="rescates_nuevo"), 
    path('rescates/<int:id>/',views.rescates_eliminar, name='rescates_eliminar'),
    path('rescates/<int:pk>/edit/',views.rescates_editar, name='rescates_editar'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)