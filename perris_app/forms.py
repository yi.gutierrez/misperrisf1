from django import forms
from .models import Persona, Mascota
from django.contrib.auth.models import User

class PersonaForm(forms.ModelForm):

    class Meta:
        model = Persona
        fields = ('email','run','nombre_completo','fechanac','telefono','regiones','comunas','tipo_vivienda','contrasenia')
        labels = {
            'email':'Correo Electrónico',
            'run':'Run',
            'nombre_completo':'Nombre Completo',
            'fechanac':'Fecha de Nacimiento',
            'telefono':'Teléfono',
            'regiones':'Región',
            'comunas':'Comuna',
            'tipo_vivienda':'Tipo de Vivienda',
            'contrasenia':'Contraseña'
            }
        widgets= {
            'email': forms.TextInput(attrs={'class':'form-control'}),
            'run': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_completo': forms.TextInput(attrs={'class':'form-control'}),
            'fechanac': forms.DateInput(attrs={'type':'date'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'regiones': forms.Select(attrs={'class':'form-control','id':'regiones'}),
            'comunas': forms.Select(attrs={'class':'form-control','id':'comunas'}),
            'tipo_vivienda': forms.Select(attrs={'class':'form-control'}),
            'contrasenia': forms.TextInput(attrs={'class':'form-control'})
        }

class UsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password',)



class MascotaForm(forms.ModelForm):

    class Meta:
        model = Mascota
        fields = ('nombre', 'raza','foto','descripcion','estado')

        labels = {
            'nombre':'Nombre',
            'raza':'Raza',
            'foto':'Fotografía',
            'descripcion':'Descripción',
            'estado':'Estado',
            }
        widgets= {
            'nombre': forms.TextInput(attrs={'class':'form-control','id':'nombre'}),
            'raza': forms.TextInput(attrs={'class':'form-control','id':'raza'}),
            'foto': forms.FileInput(attrs={'id':'foto'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control','id':'descripcion'}),
            'estado': forms.Select(attrs={'class':'form-control','id':'estado'}),
        }
